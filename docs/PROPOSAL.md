# BuyTogether

BuyTogether is a webapp that helps groups of people manage wholesale purchases.

Anyone can join BuyTogether and create a buying club, or join an existing buying club (with permission from that club's manager.) Club managers set up group buys by entering product information for each product they want to offer, including product name, price, minumum quantity and unit/increment. Club members can enter their maximum desired order quantity for a given buy. 

When the minimum quantity is reached for an order requirement, the club manager will be notified. The manager can close a buy, generate invoices for each user, and record payment and delivery status. 


## Specific Functionality

####Pages: 
* **overview / welcome / landing page**
  * display basic information about the site to non-users
* **help / contact**
  * display contact information for help (for now: me) and eventually an FAQ
* **account creation**
  * create new member account and automatically sign in when done
* **Member profile / account management**
  * dashboard-type page with information about a user's accound and memberships
* **Club creation**
* **Club profile**
  * display information from a particular Club, including current open Buys
  * this page will have a unique url based on the club's slug
* **Club management**
  * access Buy history, Buy creation, and manage status of Members
  * only available to account Owners
* **Buy creation**
* **current Buy management (including current status)**
  * change Buy status (open/closed/finalized) and Buy attributes
  * only accessible to account Owners
* **current Buy member sign-up / status**
  * sign up for a Buy, add Products by quantity
  * accessible to all Club Members
* **current Buy finalization**
  * close a Buy, adjust Member orders
  * only accessible to Club owners
* **closed Buy payment status / invoice generation**
  * generate email invoices for all or specific members, 
  * display and edit payment status for each Member on a Buy


### Data Model

A Member is a subclass of User. Members have the usual User attributes, plus a "name of use", zip code, and permissions specific to each Club they are in. Each Member will have a many-to-many relationship with the Clubs they join. Membership status and notes from the club manager are stored on the through model, Membership.

Clubs have text field for short and long descriptions and membership requirements, and zip codes. They also have a slug field so they have have a url for their profile page.

Buys have a foreign key to Clubs. Order minimums and optional open/close dates are stored on the Buy model. Product has a many-to-many relationship with Buy, and all the relevant information about Products are stored as attributes on that model. (Product name, id, description, url to further product information, weight, origin, unit and order increment.) Pricing information is stores as attributes on the through model ProductOrder, and order_total is probably a method on the model.


### Technical Components

I plan to style the site using a custom build of Bootstrap, implemented using the django-bootstrap3 package.

I'll need to automatically generate emails for Members, which apparently Django can handle.



What are the moving parts of your MVP? What are the things like "modules" you're going to write? How do they talk to each other?

_Make decisions_ here and now. Do research and prototyping to figure out what libraries and technologies will help you solve your problems. Write up which ones you'll use. Itâ€™s okay if they end up not working and you have to change your plans.

This is _more specific_ than Django backend, CSS styles, etc.â€ Please specify what specific technical problems youâ€™ll have to solve and a guess at the solution.

[Wireframes](https://en.wikipedia.org/wiki/Website_wireframe) and [flowcharts](https://en.wikipedia.org/wiki/Flowchart) are appropriate artifacts to include in your proposal. Developing those will help you think through the details of your project.

There are good tools for helping you plan your projects. The following are just examples. Look around for tools that suit how you work (and post them to Slack!)

- [wireframe.cc](https://wireframe.cc/)
- [draw.io](https://www.draw.io/)

### Schedule

I'll start buy building at least outlines of the models, which I expect to guide the direction of the rest of the project.

Next is user authentication, and the ability to create, edit, view, join, and leave groups.

Then creating and editing Buys, adding Products to Buys, and letting Members sign up for / choose Products from Buys. I expect this to be the hard part!

The last major project is to close Buys and automatically generate invoices for each Member participating. 



### Stretch goals

* Let club managers remove users from clubs.
* Schedule open and close dates for Buys
* Let club managers control permissions for individual users.
* Handle mark-up percentages
* Let club managers upload product catalogs and give users permission to add products to a buy.
* Handle payments natively
* Handle messaging natively
* Parse uploaded catalogs
* Feedback / Review system -- let club managers vouch for members, and members recommend clubs.

