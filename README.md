# BuyTogether

BuyTogether is a webapp that helps groups of users coordinate group purchases. It allows users to create clubs, open group purchases (Buys,) sign up for each other's Buys, and view their invoices.

### Setup

No special instructions for setup.

### Usage

Registered users can create new Clubs and join existing Clubs.

Club owners can edit club information and create new Buys for a club. Add Products to a Buy to make it useul! Club owners can see the total of all member's orders on any Buy.

Club members can sign up for a Buy by requesting a specific quantity of any Product on the Buy. Club members can view their invoice for a Buy from the Buy sign-up page, to see their order total.
