/*
 Collect data from join_buy.html
 */


(function app() {
    "use strict";


    function displayMessage(msg) {
        var $item = $('<li>', {'class': 'alert alert-success alert-dismissable'}).text(msg.response);
        $('.messages').html($item);
    }

    function getAjax(quantity, product) {
        $.ajax({
            url: '/buys/orderform/create/',
            type: 'POST',
            data: {
                'item_pk': product,
                'quantity': quantity
            },
            success: function (rsp) {         // Success Handler
                console.log(rsp);
                displayMessage(rsp);
            },
            error: function (err) {           // Error Handler
                console.log(err);
                displayMessage(rsp);

            }

        });
    }

    $('.quantity_form').on('change', function (evt) {
        var quantity = $(this).val();
        var product = $(this).data('product');
        getAjax(quantity, product);
    });

})();
