import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestProduct:
    def test_init(self):
        obj = mixer.blend('buys.Product')
        assert obj.pk == 1, 'Should save an instance of Product'


class TestBuy:
    def test_init(self):
        obj = mixer.blend('buys.Buy')
        assert obj.pk == 1, 'Should save an instance of Buy'


class TestMemberOrder:
    def test_init(self):
        obj = mixer.blend('buys.MemberOrder')
        assert obj.pk == 1, 'Should save an instance of through object MemberOrder'
