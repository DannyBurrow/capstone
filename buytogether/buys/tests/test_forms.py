from .. import forms
import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestClubModelForm:
    def test_form(self):
        form = forms.BuyModelForm(data={})
        assert form.is_valid() is False, 'Should be invalid if no data is given'

        club = mixer.blend('buys.Buy')
        data = club.__dict__
        form = forms.BuyModelForm(data=data)
        assert form.is_valid() is True, 'Should be valid if all data is present'