from decimal import Decimal

from accounts.models import Club, Member, Membership
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.forms import inlineformset_factory
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse

from .forms import BuyModelForm, BuyChangeForm
from .models import Buy, Product, MemberOrder


@login_required
def create_buy(request, slug):
    """ Buy creation for Club Owners """
    form = BuyModelForm(request.POST or None)
    club = get_object_or_404(Club, slug=slug)

    if form.is_valid():
        buy = form.save(commit=False)
        buy.club = club
        buy.save()

        messages.add_message(request, messages.SUCCESS, 'Buy has successfully been created.')

        return redirect(reverse('manage_club', kwargs={'slug': slug}))

    context = {'form': form}
    return render(request, 'pages/buy_creation.html', context)


@login_required
def manage_buy(request, pk):
    """ Buy management page for Club Owners """
    buy = Buy.objects.get(pk=pk)
    memberstatus = Membership.objects.get(member=request.user, club=buy.club)

    if memberstatus.status != 'OW':
        return redirect(reverse('login'))

    ProductFormSet = inlineformset_factory(Buy, Product, extra=3, can_delete=True, exclude=('order_min',))
    formset = ProductFormSet(instance=buy)

    update_form = BuyChangeForm(data=request.POST or None, instance=buy)

    if update_form.is_valid():
        messages.add_message(request, messages.SUCCESS, 'Buy has successfully been updated.')

    context = {'buy': buy, 'formset': formset, 'form': update_form}
    return render(request, 'pages/buy_management.html', context)


def add_to_buy(request, pk):
    """ Add Products to Buys for Club Owners"""
    buy = Buy.objects.get(pk=pk)

    ProductFormSet = inlineformset_factory(Buy, Product, extra=3, can_delete=True, exclude=('order_min',))

    if request.method == 'POST':
        formset = ProductFormSet(data=request.POST, instance=buy)

        if formset.is_valid():
            instances = formset.save(commit=False)
            for i in instances:                       # New objects
                i.buy = buy
                i.save()

            for form in formset.deleted_forms:        # Deleted objects.
                form.instance.delete()

            messages.add_message(request, messages.SUCCESS, 'Products have successfully been added.')

            return redirect(reverse('manage_buy', kwargs={'pk': buy.pk}))


def update_buy(request, pk):
    """ Change Buy information """

    buy = Buy.objects.get(pk=pk)
    form = BuyChangeForm(data=request.POST or None, instance=buy)
    if form.is_valid():
        updated = form.save(commit=False)
        updated.save()
        return redirect(reverse('manage_buy', kwargs={'pk': buy.pk}))


@login_required
def member_order_status(request, pk):
    """ Club Members view and update existing MemberOrder objects """

    buy = Buy.objects.get(pk=pk)                                            # This Buy
    buy_products = Product.objects.filter(buy=buy)                          # Products on this buy.
    member_orders = MemberOrder.objects.filter(order__in=buy_products, member=request.user)  # MOs related to products.

    products = []   # Make a list of the products that request.user does not have a MO for.
    for p in buy_products:
        if p.pk not in member_orders.values_list('order__pk', flat=True):
            products.append(p)

    OrderFormset = inlineformset_factory(Member, MemberOrder, fields=('order_quantity',), extra=0)

    # Filters for MemberOrders related to request.user
    formset = OrderFormset(instance=request.user, queryset=member_orders)

    context = {'products': products, 'buy': buy, 'formset': formset}
    return render(request, 'pages/join_buy.html', context)


@login_required
def add_item(request):
    """ Club Members create new MemberOrder objects """

    if request.method == 'POST':
        order_pk = request.POST.get('item_pk')
        quantity = request.POST.get('quantity', 0)
        product = Product.objects.get(pk=order_pk)

        defaults = {'order_quantity': quantity}

        order, created = MemberOrder.objects.update_or_create(member=request.user, order=product, defaults=defaults)

        if created:
            data = {'response': 'Order for {} units of {} created'.format(quantity, product.name)}
        else:
            """ Delete MemberOrder instance if quantity is set to 0 """
            if Decimal(order.order_quantity) == Decimal('0'):
                order.delete()
                data = {'response': 'Order for {} deleted'.format(product.name)}
            else:
                data = {'response': 'Order for {} units of {} updated'.format(quantity, product.name)}

    return JsonResponse(data=data)


def invoice(request, buy_pk, member_pk):
    member = Member.objects.get(pk=member_pk)
    buy = Buy.objects.get(pk=buy_pk)

    products = buy.products.all()

    orders = []

    for p in products:
        order = MemberOrder.objects.filter(member=member, order=p)
        orders.extend(order)

    invoice_total = sum(orders)

    context = {"member": member, "buy": buy, 'products': products, 'orders': orders, 'invoice_total': invoice_total}
    return render(request, 'pages/invoice.html', context)
