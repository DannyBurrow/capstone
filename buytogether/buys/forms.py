from django import forms

from .models import Buy


class BuyModelForm(forms.ModelForm):
    class Meta:
        model = Buy
        fields = ('name', 'date_open', 'date_close', 'notes', 'order_min_dollar',
                  'order_min_weight', 'order_max_weight', 'order_weight_unit')


class BuyChangeForm(forms.ModelForm):
    class Meta:
        model = Buy
        fields = ('date_open', 'date_close', 'notes', 'order_min_dollar',
                  'order_min_weight', 'order_max_weight', 'order_weight_unit')
