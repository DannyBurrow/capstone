from django.db import models


class Buy(models.Model):
    STATUS_OPTIONS = {
        ('PD', 'Pending'),
        ('OP', 'Open'),
        ('CL', 'Closed'),
        ('FN', 'Finished'),
    }

    name = models.CharField(max_length=100)
    club = models.ForeignKey('accounts.Club', related_name='buys', blank=True)
    status = models.CharField(max_length=2, choices=STATUS_OPTIONS, default='OP')
    date_open = models.DateTimeField(auto_now=False)
    date_close = models.DateTimeField(auto_now=False)
    notes = models.TextField(max_length=1000, null=True, blank=True)
    order_min_dollar = models.FloatField(null=True, blank=True)
    order_min_weight = models.FloatField(null=True, blank=True)
    order_max_weight = models.FloatField(null=True, blank=True)
    order_weight_unit = models.CharField(max_length=10, null=True, blank=True)

    def order_total(self):
        """ The dollar value of all orders for every Product on This Buy """
        products = Product.objects.filter(buy=self)
        total = sum(p.order_total() for p in products)
        return total

    def __str__(self):
        return self.name

    def __repr__(self):
        return 'Buy object: {}, in Club: {}'.format(self.name, self.club)


class Product(models.Model):
    name = models.CharField(max_length=200, blank=True)
    buy = models.ForeignKey(Buy, related_name='products', blank=True, null=False)
    members = models.ManyToManyField('accounts.Member', related_name='products', blank=True)
    item_id = models.CharField(max_length=50, null=True, blank=True)
    description = models.CharField(max_length=1000, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    origin = models.CharField(max_length=100, null=True, blank=True)
    unit = models.CharField(max_length=10, null=True, blank=True)
    unit_price = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True)
    case_price = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True)
    weight = models.FloatField(null=True, blank=True)
    weight_unit = models.FloatField(null=True, blank=True)
    order_min = models.FloatField(null=True, blank=True)

    def order_total(self):
        """ The dollar value of all orders for this Products on This Buy """
        memberorders = MemberOrder.objects.filter(order=self)
        total = sum(mo.order_total() for mo in memberorders)
        return total

    def order_count(self):
        """ The unit count of all orders for this Products on This Buy """
        memberorders = MemberOrder.objects.filter(order=self)
        total = sum(mo.order_quantity for mo in memberorders)
        return total

    def __str__(self):
        return self.name

    def __repr__(self):
        return 'Product object: {} in Buy: {}'.format(self.name, self.buy)


class MemberOrder(models.Model):
    """ Through model for Product and accounts.Member """

    member = models.ForeignKey('accounts.Member')
    order = models.ForeignKey(Product)

    order_quantity = models.DecimalField(decimal_places=2, max_digits=8, default=0)
    order_date = models.DateTimeField(auto_now=True)

    def order_total(self):
        """ The total cost of this MemberOrder """
        total = self.order.unit_price * self.order_quantity
        return total

    def __add__(self, other):
        return self.order_total() + other.order_total()

    def __radd__(self, other):
        return self.order_total() + other

    def __str__(self):
        return 'Member: {}, Product: {}, Quantity: {}'.format(self.member, self.order, self.order_quantity)

    def __repr__(self):
        return 'MemberOrder object for Member: {}, Product: {}'.format(self.member, self.order)
