from django.contrib import admin
from .models import Buy, Product, MemberOrder


class ProductInlineAdmin(admin.TabularInline):
    model = Product


class BuyAdmin(admin.ModelAdmin):
    model = Buy
    inlines = [ProductInlineAdmin, ]


class MemberOrderInlineAdmin(admin.TabularInline):
    model = MemberOrder


class ProductAdmin(admin.ModelAdmin):
    model = Product
    inlines = [MemberOrderInlineAdmin, ]
    list_display = ('name', 'item_id')


admin.site.register(Product, ProductAdmin)
admin.site.register(Buy, BuyAdmin)
admin.site.register(MemberOrder)
