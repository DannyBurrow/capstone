from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.decorators import login_required
from django.core.exceptions import FieldError
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import View
from accounts.models import Club, Membership
from buys.models import Product, Buy, MemberOrder
from .forms import ClubModelForm, MemberCreationForm, ClubChangeForm


def welcome(request):
    """ Home page """
    context = {}
    return render(request, 'pages/welcome.html', context)


def contact(request):
    """ Contact, Help, FAQ """
    context = {'member': request.user}
    return render(request, 'pages/contact.html', context)


@login_required()
def member_home(request):
    """ Member home dashboard """
    memberships = Membership.objects.filter(member=request.user)
    context = {'memberships': memberships}
    return render(request, 'pages/memberhome.html', context)


class MemberCreateView(View):
    """ User sign-up """

    def get(self, request):
        form = MemberCreationForm()
        context = {'form': form}
        return render(request, 'pages/membercreation.html', context)

    def post(self, request):
        form = MemberCreationForm(data=request.POST)

        if form.is_valid():
            form.save(commit=True)

            member = authenticate(username=request.POST['email'], password=request.POST['password1'])
            if member is not None:
                auth_login(request, member)
                return redirect('/accounts/memberhome/')

        context = {'form': form}
        return render(request, 'pages/membercreation.html', context)


@login_required()
def create_club(request):
    """ Club creation """
    form = ClubModelForm(request.POST or None)

    if form.is_valid():
        club = form.save(commit=False)
        club.save()

        membership = Membership(member=request.user, club=club, status='OW')
        membership.save()

        return redirect(reverse('manage_club', kwargs={'slug': club.slug}))

    context = {'form': form}
    return render(request, 'pages/clubcreation.html', context)


@login_required()
def join_club(request):
    """ Non-owner join existing club """
    if request.method == 'POST':
        slug = request.POST['slug']
        club = Club.objects.get(slug=slug)

        membership = Membership(member=request.user, club=club, status='AV')
        membership.save()

        data = {'results': 'Joined {}'.format(club.name)}
        return JsonResponse(data=data)


def leave_club(request):
    """ Non-owner change Membership status to 'self-removed' """
    if request.method == 'POST':
        slug = request.POST['slug']
        club = Club.objects.get(slug=slug)

        membership = Membership.objects.get(club=club, member=request.user)
        membership.status = 'LF'
        membership.save()

        data = {'results': 'Left {}'.format(club.name)}
        return JsonResponse(data=data)


def club_profile(request, slug):
    """ Club profile page for the public """
    club = get_object_or_404(Club, slug=slug)
    owner = get_object_or_404(Membership, status='OW', club=club)
    memberships = Membership.objects.filter(club=club)
    open_buys = Buy.objects.filter(club=club, status='OP')

    context = {'club': club, 'owner': owner, 'memberships': memberships, 'buys': open_buys}

    if request.user.is_authenticated():

        if request.user in club.members.all():
            status = Membership.objects.get(club=club, member=request.user).status

            if status in ['AV', 'AD', 'IN']:
                club_status = 'unjoin'
                button_text = 'Leave Club'

            elif status in ['LF']:
                club_status = 'join_club'
                button_text = 'Join Club'

            elif status in ['RQ']:
                club_status = 'requested'
                button_text = 'Your Request Has Been Submitted'

            elif status in ['OW']:
                club_status = 'manage'
                button_text = 'Manage this Club'

            elif status:
                club_status = None
                button_text = 'You shouldn\'t be seeing this.'

            else:
                raise FieldError('No status on accounts.Membership. Memberships must have a status.')

        else:
            club_status = 'join_club'
            button_text = 'Join Club'

        context.update({'button': button_text, 'status': club_status})

    return render(request, 'pages/club_profile.html', context)


@login_required()
def manage_club(request, slug):
    """ Club management page for owners """
    club = get_object_or_404(Club, slug=slug)
    user = Membership.objects.get(club=club, member=request.user)
    members = Membership.objects.filter(club=club)
    buys = club.buys.all()

    if user.status != 'OW':
        return redirect('/accounts/memberhome/')

    form = ClubChangeForm(data=request.POST or None, instance=club)
    if form.is_valid():
        club = form.save(commit=False)
        club.save()

    context = {'club': club, 'members': members, 'form': form, 'buys': buys}
    return render(request, 'pages/club_management.html', context)
