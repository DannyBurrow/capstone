import pytest
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from .. import views
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestMemberCreateView:
    def test_anonymous(self):
        req = RequestFactory().get('/')
        resp = views.MemberCreateView.as_view()(req)
        assert resp.status_code == 200, 'Should be callable by anyone'


class TestWelcome:
    def test_welcome(self):
        req = RequestFactory().get('/')
        resp = views.welcome(req)
        assert resp.status_code == 200, 'Should be callable by anyone'


class TestMemberHome:
    def test_anonymous(self):
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        resp = views.member_home(req)
        assert resp.status_code == 302, 'Should not be callable by non-owner'
        assert 'login' in resp.url, 'Should redirect to login'


class TestManageClubView:

    def test_anonymous(self):
        req = RequestFactory().get('/')
        req.user = AnonymousUser()
        club = mixer.blend('accounts.Club')

        resp = views.manage_club(req, club.slug)
        assert 'login' in resp.url, 'Should redirect to login'

    def test_owner(self):
        user = mixer.blend('accounts.Member', is_superuser=True)
        club = mixer.blend('accounts.Club')
        membership = mixer.blend('accounts.Membership', member=user, club=club, status='OW')
        buys = mixer.blend('buys.Buy')
        req = RequestFactory().get('/')
        req.user = user
        resp = views.manage_club(req, club.slug)

        assert resp.status_code == 200, 'Should be callable by superuser/club owner'

    def test_not_owner(self):
        user = mixer.blend('accounts.Member')
        club = mixer.blend('accounts.Club')
        membership = mixer.blend('accounts.Membership', member=user, club=club, status='AV')
        buys = mixer.blend('buys.Buy')
        req = RequestFactory().get('/')
        req.user = user
        resp = views.manage_club(req, club.slug)

        assert resp.status_code == 302, 'Should not be callable by non-owner'
