import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestMember:
    def test_init(self):
        obj = mixer.blend('accounts.Member')
        assert obj.pk == 1, 'Should save an instance of Member'


class TestClub:
    def test_init(self):
        obj = mixer.blend('accounts.Club')
        assert obj.pk == 1, 'Should save an instance of Club'


class TestMembership:
    def test_init(self):
        obj = mixer.blend('accounts.Membership')
        assert obj.pk == 1, 'Should save an instance of Membership'

