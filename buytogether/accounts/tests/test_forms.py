from .. import forms
import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestMemberCreationForm:
    def test_form(self):
        form = forms.MemberCreationForm(data={})
        assert form.is_valid() is False, 'Should be invalid if no data is given'

        member = mixer.blend('accounts.Member')
        data = member.__dict__
        data['password1'] = 'llamas12'
        data['password2'] = 'llamas12'
        form = forms.MemberCreationForm(data=data)
        assert form.is_valid() is True, 'Should be valid if all data is present'


class TestClubModelForm:
    def test_form(self):
        form = forms.ClubModelForm(data={})
        assert form.is_valid() is False, 'Should be invalid if no data is given'

        club = mixer.blend('accounts.Club')
        data = club.__dict__
        form = forms.ClubModelForm(data=data)
        assert form.is_valid() is True, 'Should be valid if all data is present'


class TestClubChangeForm:
    def test_form(self):
        form = forms.ClubChangeForm(data={})
        assert form.is_valid() is False, 'Should be invalid if no data is given'

        data = {'desc_brief': "Description", 'zip_code': '5'}
        form = forms.ClubChangeForm(data=data)
        assert form.is_valid() is False, 'Should require a valid zip code for validity'

        data = {'desc_brief': "Description", 'zip_code': '83501'}
        form = forms.ClubChangeForm(data=data)
        assert form.is_valid() is True, 'Should be valid if all data is present'
