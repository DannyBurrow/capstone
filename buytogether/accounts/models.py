from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.text import slugify
from localflavor.us.models import USZipCodeField


class Member(AbstractUser):
    """ ManyToManyField on buys.Product """
    name_of_use = models.CharField(max_length=150)
    zip_code = USZipCodeField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.username = self.email
        super().save(*args, **kwargs)

    def __str__(self):
        return "User: {}, {}".format(self.name_of_use, self.email)

    def __repr__(self):
        return "User object: {}".format(self.username)


class Club(models.Model):
    members = models.ManyToManyField(Member, through='Membership', related_name='clubs')
    name = models.CharField(max_length=150)
    slug = models.SlugField(null=False, blank=True)
    zip_code = USZipCodeField(null=True, blank=True)
    desc_brief = models.TextField(max_length=200)
    desc_verbose = models.TextField(max_length=10000, null=True, blank=True)
    requirements = models.TextField(max_length=500,
                                    help_text='Explain the requirements.',
                                    null=True, blank=True)

    def __str__(self):
        return "Club: {}".format(self.name)

    def __repr__(self):
        return "{}".format(self.name)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


class Membership(models.Model):
    """ Through model for Club and Member """
    STATUS_OPTIONS = {
        ('OW', 'Owner'),
        ('AD', 'Administrator'),
        ('AV', 'Active'),
        ('IN', 'Inactive'),
        ('LF', 'Self-removed'),
        ('RQ', 'Requested'),
        ('RJ', 'Rejected'),
        ('RM', 'Removed'),
    }

    member = models.ForeignKey(Member)
    club = models.ForeignKey(Club)
    notes = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=2, choices=STATUS_OPTIONS, default='AV')
    date_joined = models.DateTimeField(auto_now=True)
    date_removed = models.DateTimeField(auto_now=False, null=True, blank=True)
    reason_removed = models.TextField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return "{}'s membership in {}".format(self.member.username, self.club.name)

    def __repr__(self):
        return "User: {}. Club: {}".format(self.member.username, self.club.name)
