from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import Member, Club


class MemberCreationForm(UserCreationForm):
    class Meta:
        model = Member
        fields = ('email', 'name_of_use', 'zip_code', 'password1', 'password2')


class ClubModelForm(forms.ModelForm):
    class Meta:
        model = Club
        fields = ('name', 'desc_brief', 'desc_verbose', 'requirements', 'zip_code')
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'This is the name of your club. It cannot be changed.'}),
            'description': forms.Textarea(attrs={'placeholder': 'Look for widgets in forms.py'})
                   }


class ClubChangeForm(forms.ModelForm):
    class Meta:
        model = Club
        fields = ('desc_brief', 'desc_verbose', 'requirements', 'zip_code')

