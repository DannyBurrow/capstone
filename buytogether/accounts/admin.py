from django.contrib import admin
from .models import Member, Club, Membership


class MembershipInline(admin.TabularInline):
    model = Club.members.through


class ClubAdmin(admin.ModelAdmin):
    inlines = [MembershipInline]


class MemberAdmin(admin.ModelAdmin):
    model = Member
    list_display = ('name_of_use', 'clubs', 'zip_code', 'last_login', 'date_joined')


admin.site.register(Member, MemberAdmin)
admin.site.register(Club, ClubAdmin)
admin.site.register(Membership)
