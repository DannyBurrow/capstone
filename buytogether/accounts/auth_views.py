from django.contrib.auth import authenticate
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import redirect, render
from django.views.generic import RedirectView


def login_view(request):
    """
    Logs users in and redirects them to Member Home on success.
    """

    if request.method == 'GET':
        form = AuthenticationForm()

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/accounts/memberhome')
        else:
            form = AuthenticationForm(data=request.POST)

    context = {'form': form}
    return render(request, 'pages/login.html', context)


class LogoutView(RedirectView):
    """
    Logs users out and redirects them to Home.
    """
    url = '/home'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)
