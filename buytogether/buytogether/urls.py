"""buytogether URL Configuration

The `urlpatterns` list routes URLs to account_views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Functionaccount_views
    1. Add an import:  from my_app importaccount_views
    2. Add a URL to urlpatterns:  url(r'^$', account_views.home, name='home')
Class-basedaccount_views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from accounts import views as account_views
from accounts.auth_views import login_view, LogoutView
from buys import views as buy_views
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^home/', account_views.welcome, name='welcome'),
    url(r'^tester/', account_views.trystuff),
    url(r'^contact/', account_views.contact, name='contact'),

    # Accounts URLs
    url(r'^accounts/login/', login_view, name='login'),
    url(r'^accounts/logout/', LogoutView.as_view(), name='logout'),

    url(r'^accounts/newuser/', account_views.MemberCreateView.as_view(), name='new_user'),
    url(r'^accounts/memberhome/', account_views.member_home, name='member_home'),

    url(r'^accounts/newclub/', account_views.create_club, name='new_club'),
    url(r'^accounts/(?P<slug>[a-z\-]+)/$', account_views.club_profile, name='club_profile'),
    url(r'^accounts/(?P<slug>[a-z\-]+)/manage/', account_views.manage_club, name='manage_club'),
    url(r'^accounts/club/join/', account_views.join_club, name='join'),
    url(r'^accounts/club/unjoin/', account_views.leave_club, name='unjoin'),

    # Buys URLs
    url(r'^buys/(?P<slug>[a-z\-]+)/addbuy/', buy_views.create_buy, name='add_buy'),
    url(r'^buys/(?P<pk>\d+)/manage/', buy_views.manage_buy, name='manage_buy'),
    url(r'^buys/(?P<pk>\d+)/add/', buy_views.add_to_buy, name='add_to_buy'),
    url(r'^buys/(?P<pk>\d+)/update/$', buy_views.update_buy, name='update_buy'),

    url(r'^buys/(?P<pk>\d+)/member_order/', buy_views.member_order_status, name='member_order_status'),
    url(r'^buys/orderform/create/', buy_views.add_item, name='add_item'),
    url(r'^buys/invoice/(?P<buy_pk>\d+)/(?P<member_pk>\d+)', buy_views.invoice, name='invoice'),

    # Boilerplate REST urls
    url(r'^api/v1/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

